<div class="wrap">
    <h2><?php esc_attr_e( 'FaceBook Pixel Feed', 'WpAdminStyle' ); ?></h2>
</div>



<form method="POST" action="">

<ul class="wa_fb_tabs">

    <li>
        <input type="radio" name="wa_fb_tabs" id="tab2" checked/>
        <label class="wa_fb-tab-name" for="tab2"><?php echo _e('Wordpress', ''); ?></label>
        <div id="wa_fb-tab-content2" class="wa_fb-tab-content">
                <table class="wa_fb_pixel_id">
                    <tbody>
                        <tr>
                           <!-- <td> <label> Facebook Pixel Id:</label> </td>-->
                            <td> <input type="text" name="wa_fb_pixel_id"  id="wa_fb_pixel_id" value="<?php echo  get_option('wa_fb_pixel_id'); ?>" placeholder="Facebook Pixel Id"/> </td>
                        </tr>
                        <tr>
                            <td>  <label for="">Remove Pixel For User:</label> </td>
                        </tr>
                        <?php
                        $editable_roles = array_reverse( get_editable_roles() );
                        foreach ( $editable_roles as $role => $details ) {
                            $name = translate_user_role($details['name'] ); ?>

                            <tr>

                                <td> <label for=""><?php echo $name;?></label> </td>
                                <td> <input type="checkbox" name="" id=""/> Enable </td>
                            </tr>
                        <?php } ?>

                        <tr>
                            <td>Track Pages</td>
                            <td> <input type="checkbox"> Pages  <input type="checkbox"> Posts  <input type="checkbox"> Product</td>
                        </tr>


                        <tr>
                            <td>
                                <!--<input type="submit" value="Save Changels" class="wa_fb_btn_submit"/>-->
                            </td>
                        </tr>
                    </tbody>
                </table>
        </div>
    </li>


    <li>
        <input type="radio" name="wa_fb_tabs" id="tab1" checked/>
        <label class="wa_fb-tab-name" for="tab1"><?php echo _e('Woocommerce', ''); ?></label>
        <div id="wa_fb-tab-content1" class="wa_fb-tab-content">
            <table class="table widefat fixed mtable">
                <tbody>
                    <tr>
                        <td>
                            <label for="" class="wa_fb_woo_label">Search</label>
                            <input type="checkbox" class="wa_fb_woo" name="wa_fb_searchcontent" id="wa_fb_searchcontent" <?php echo get_option('wa_fb_searchcontent');?>
                                <?php checked('on',get_option('wa_fb_searchcontent'),true,'checked'); ?>/>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label for="" class="wa_fb_woo_label">View Content</label>
                            <input type="checkbox" class="wa_fb_woo" name="wa_fb_viewcontent" id="wa_fb_viewcontent" <?php checked('on',get_option('wa_fb_viewcontent'),true,'checked'); ?>/>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label for="" class="wa_fb_woo_label">Add To Cart</label>
                            <input type="checkbox" class="wa_fb_woo" name="wa_fb_addtocart" id="wa_fb_addtocart" <?php checked('on',get_option('wa_fb_addtocart'),true,'checked'); ?> />
                        </td>
                    </tr>
                    <tr>
                        <td><label for="" class="wa_fb_woo_label">Add To Wishlist</label>
                            <input type="checkbox" class="wa_fb_woo" name="wa_fb_addwishlist" id="wa_fb_addwishlist" <?php checked('on',get_option('wa_fb_addwishlist'),true,'checked'); ?> />
                        </td>
                    </tr>
                    <tr>
                        <td> <label for="" class="wa_fb_woo_label">Initial Checkout</label>
                            <input type="checkbox" class="wa_fb_woo"  name="wa_fb_InitiateCheckout" id="wa_fb_InitiateCheckout" <?php checked('on',get_option('wa_fb_InitiateCheckout'),true,'checked'); ?> />
                        </td>
                    </tr>
                    <tr>
                        <td>  <label for="" class="wa_fb_woo_label">Add PaymentInfo</label>
                            <input type="checkbox" class="wa_fb_woo"  name="wa_fb_AddPaymentInfo" id="wa_fb_AddPaymentInfo" <?php checked('on',get_option('wa_fb_AddPaymentInfo'),true,'checked'); ?>  />
                        </td>
                    </tr>
                    <tr>
                        <td>  <label for="" class="wa_fb_woo_label">Purchase</label>
                            <input type="checkbox" class="wa_fb_woo" name="wa_fb_Purchase" id="wa_fb_Purchase" <?php checked('on',get_option('wa_fb_Purchase'),true,'checked'); ?>  />
                        </td>
                    </tr>
                    <tr>
                        <td><label for="" class="wa_fb_woo_label">Lead</label>
                            <input type="checkbox" class="wa_fb_woo"  name="wa_fb_Lead" id="wa_fb_Lead" <?php checked('on',get_option('wa_fb_Lead'),true,'checked'); ?> />
                        </td>
                    </tr>
                    <tr>
                        <td>  <label for="" class="wa_fb_woo_label">Complete Registration</label>
                            <input type="checkbox" class="wa_fb_woo"  name="wa_fb_CompleteRegistration" id="wa_fb_CompleteRegistration" <?php checked('on',get_option('wa_fb_CompleteRegistration'),true,'checked'); ?>/>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </li>
    <li>
        <input type="radio" name="wa_fb_tabs" id="tab3"/>
        <label class="wa_fb-tab-name" for="tab3"><?php echo _e('Easy Digital Download', 'woo-feed'); ?></label>

        <div id="wa_fb-tab-content3" class="wa_fb-tab-content">
            <table class="table widefat fixed mtable">
                <tbody>
                <tr>
                    <td>
                        <label for="" class="wa_fb_edd_label">Search</label>
                        <input type="checkbox" class="wa_fb_edd" name="wa_fb_searchcontent" id="wa_fb_searchcontent" <?php echo get_option('wa_fb_searchcontent');?>
                            <?php checked('on',get_option('wa_fb_searchcontent'),true,'checked'); ?>/>
                    </td>
                </tr>
                <tr>
                    <td>
                        <label for="" class="wa_fb_edd_label">View Content</label>
                        <input type="checkbox" class="wa_fb_edd" name="wa_fb_viewcontent" id="wa_fb_viewcontent" <?php checked('on',get_option('wa_fb_viewcontent'),true,'checked'); ?>/>
                    </td>
                </tr>
                <tr>
                    <td>
                        <label for="" class="wa_fb_edd_label">Add To Cart</label>
                        <input type="checkbox" class="wa_fb_edd" name="wa_fb_addtocart" id="wa_fb_addtocart" <?php checked('on',get_option('wa_fb_addtocart'),true,'checked'); ?> />
                    </td>
                </tr>
                <tr>
                    <td><label for="" class="wa_fb_edd_label">Add To Wishlist</label>
                        <input type="checkbox" class="wa_fb_edd" name="wa_fb_addwishlist" id="wa_fb_addwishlist" <?php checked('on',get_option('wa_fb_addwishlist'),true,'checked'); ?> />
                    </td>
                </tr>
                <tr>
                    <td> <label for="" class="wa_fb_edd_label">Initial Checkout</label>
                        <input type="checkbox" class="wa_fb_edd"  name="wa_fb_InitiateCheckout" id="wa_fb_InitiateCheckout" <?php checked('on',get_option('wa_fb_InitiateCheckout'),true,'checked'); ?> />
                    </td>
                </tr>
                <tr>
                    <td>  <label for="" class="wa_fb_edd_label">Add PaymentInfo</label>
                        <input type="checkbox" class="wa_fb_edd"  name="wa_fb_AddPaymentInfo" id="wa_fb_AddPaymentInfo" <?php checked('on',get_option('wa_fb_AddPaymentInfo'),true,'checked'); ?>  />
                    </td>
                </tr>
                <tr>
                    <td>  <label for="" class="wa_fb_edd_label">Purchase</label>
                        <input type="checkbox" class="wa_fb_edd" name="wa_fb_Purchase" id="wa_fb_Purchase" <?php checked('on',get_option('wa_fb_Purchase'),true,'checked'); ?>  />
                    </td>
                </tr>
                <tr>
                    <td><label for="" class="wa_fb_edd_label">Lead</label>
                        <input type="checkbox" class="wa_fb_edd"  name="wa_fb_Lead" id="wa_fb_Lead" <?php checked('on',get_option('wa_fb_Lead'),true,'checked'); ?> />
                    </td>
                </tr>
                <tr>
                    <td>  <label for="" class="wa_fb_edd_label">Complete Registration</label>
                        <input type="checkbox" class="wa_fb_edd"  name="wa_fb_CompleteRegistration" id="wa_fb_CompleteRegistration" <?php checked('on',get_option('wa_fb_CompleteRegistration'),true,'checked'); ?>/>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
    </li>

    <li>
        <input type="radio" name="wa_fb_tabs" id="tab4" checked/>
        <label class="wa_fb-tab-name" for="tab4"><?php echo _e('Custom Event', ''); ?></label>
        <div id="wa_fb-tab-content4" class="wa_fb-tab-content">
            <a href="javascript:void(0)" id="loginlink">Add Event</a>
        </div>
    </li>

</ul>

</form>



<div class="wrap">
    <div id="wa_fb_mymodal" class="wa_fb_customevent_modal">
        <div class="wa_fb_customevent_model_content">
            <span class="wa_fb_close">&times;</span>

            <div class="wa_fb_popup_border">
                <form>

                    <table class="wa_fb_custom_event_table table widefat fixed mtable" style="border: none">
                        <tr>
                            <td>URL</td>
                            <td><input type="text" name="" id="" class="wa_fb_input"/></td>
                        </tr>
                        <tr>
                            <td>
                                <label for="">Event Type</label>
                            </td>
                            <td>
                                <select id="wa_fb_customevent_state">
                                    <option>Select Type</option>
                                    <option value="1">Custom Event</option>
                                    <option value="2">Custom Event Code</option>
                                </select>
                            </td>
                        </tr>
                    </table>
                </form>
            </div>
        </div>
    </div>
</div>


<div class=" widefat fixed">
            <button type="submit" id="wf_submit" class="wa_fb_btn_submit">
                <?php echo _e('Save & Generate Feed', 'woo-feed'); ?>
            </button>
</div>