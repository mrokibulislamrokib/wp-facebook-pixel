<div class="wrap">
    <h2><?php esc_attr_e( 'Form Elements: Input Fields', 'WpAdminStyle' ); ?></h2>
    <form method="POST" action="">
        <label for="type">Tracking Type</label>
        <select name="wa_fb_type" id="wa_fb_type">
            <option value="1" <?php selected( get_option('wa_fb_type'), 1 );  ?> >Basic</option>
            <option value="2" <?php selected( get_option('wa_fb_type'), 2 );  ?> >Recommended</option>
            <option value="3" <?php selected( get_option('wa_fb_type'), 3 );  ?> >Advance</option>
        </select>
        <label for="">Search</label>
        <input type="checkbox" class="wa_fb_woo" name="wa_fb_searchcontent" id="wa_fb_searchcontent" <?php echo get_option('wa_fb_searchcontent');?>
            <?php checked('on',get_option('wa_fb_searchcontent'),true,'checked'); ?>/>
        <label for="">View Content</label>
        <input type="checkbox" class="wa_fb_woo" name="wa_fb_viewcontent" id="wa_fb_viewcontent" <?php checked('on',get_option('wa_fb_viewcontent'),true,'checked'); ?>/>
        <label for="">Add To Cart</label>
        <input type="checkbox" class="wa_fb_woo" name="wa_fb_addtocart" id="wa_fb_addtocart" <?php checked('on',get_option('wa_fb_addtocart'),true,'checked'); ?> />
        <label for="">Add To Wishlist</label>
        <input type="checkbox" class="wa_fb_woo" name="wa_fb_addwishlist" id="wa_fb_addwishlist" <?php checked('on',get_option('wa_fb_addwishlist'),true,'checked'); ?> />
        <label for="">Initial Checkout</label>
        <input type="checkbox" class="wa_fb_woo"  name="wa_fb_InitiateCheckout" id="wa_fb_InitiateCheckout" <?php checked('on',get_option('wa_fb_InitiateCheckout'),true,'checked'); ?> />
        <label for="">Add PaymentInfo</label>
        <input type="checkbox" class="wa_fb_woo"  name="wa_fb_AddPaymentInfo" id="wa_fb_AddPaymentInfo" <?php checked('on',get_option('wa_fb_AddPaymentInfo'),true,'checked'); ?>  />
        <label for="">Purchase</label>
        <input type="checkbox" class="wa_fb_woo" name="wa_fb_Purchase" id="wa_fb_Purchase" <?php checked('on',get_option('wa_fb_Purchase'),true,'checked'); ?>  />
        <label for="">Lead</label>
        <input type="checkbox" class="wa_fb_woo"  name="wa_fb_Lead" id="wa_fb_Lead" <?php checked('on',get_option('wa_fb_Lead'),true,'checked'); ?> />
        <label for="">Complete Registration</label>
        <input type="checkbox" class="wa_fb_woo"  name="wa_fb_CompleteRegistration" id="wa_fb_CompleteRegistration" <?php checked('on',get_option('wa_fb_CompleteRegistration'),true,'checked'); ?>/>
        <input type="submit" value="Save" name="wa_fb_submit"/>
        <?php // submit_button();?>
    </form>
    <button id="checkAll">CheckAll</button>
</div>

