<div class="wrap">
    <h2><?php esc_attr_e( 'Form Elements: Input Fields', 'WpAdminStyle' ); ?></h2>
    <form method="POST" action="">
        <label for="type">Tracking Type</label>
        <select name="wa_fb_edd_type" id="wa_fb_edd_type">
            <option value="1" <?php selected( get_option('wa_fb_edd_type'), 1 );  ?> >Basic</option>
            <option value="2" <?php selected( get_option('wa_fb_edd_type'), 2 );  ?> >Recommended</option>
            <option value="3" <?php selected( get_option('wa_fb_edd_type'), 3 );  ?> >Advance</option>
        </select>
        <label for="">Search</label>
        <input type="checkbox" name="wa_fb_edd_searchcontent" id="wa_fb_edd_searchcontent" <?php echo get_option('wa_fb_edd_searchcontent');?>
            <?php checked('on',get_option('wa_fb_edd_searchcontent'),true,'checked'); ?>/>
        <label for="">View Content</label>
        <input type="checkbox" name="wa_fb_edd_viewcontent" id="wa_fb_edd_viewcontent" <?php checked('on',get_option('wa_fb_edd_viewcontent'),true,'checked'); ?>/>
        <label for="">Add To Cart</label>
        <input type="checkbox" name="wa_fb_edd_addtocart" id="wa_fb_edd_addtocart" <?php checked('on',get_option('wa_fb_edd_addtocart'),true,'checked'); ?> />
        <label for="">Add To Wishlist</label>
        <input type="checkbox" name="wa_fb_edd_addwishlist" id="wa_fb_edd_addwishlist" <?php checked('on',get_option('wa_fb_edd_addwishlist'),true,'checked'); ?> />
        <label for="">Initial Checkout</label>
        <input type="checkbox" name="wa_fb_edd_InitiateCheckout" id="wa_fb_edd_InitiateCheckout" <?php checked('on',get_option('wa_fb_edd_InitiateCheckout'),true,'checked'); ?> />
        <label for="">Add PaymentInfo</label>
        <input type="checkbox" name="wa_fb_edd_AddPaymentInfo" id="wa_fb_edd_AddPaymentInfo" <?php checked('on',get_option('wa_fb_edd_AddPaymentInfo'),true,'checked'); ?>  />
        <label for="">Purchase</label>
        <input type="checkbox" name="wa_fb_edd_Purchase" id="wa_fb_edd_Purchase" <?php checked('on',get_option('wa_fb_edd_Purchase'),true,'checked'); ?>  />
        <label for="">Lead</label>
        <input type="checkbox" name="wa_fb_edd_Lead" id="wa_fb_edd_Lead" <?php checked('on',get_option('wa_fb_edd_Lead'),true,'checked'); ?> />
        <label for="">Complete Registration</label>
        <input type="checkbox" name="wa_fb_edd_CompleteRegistration" id="wa_fb_edd_CompleteRegistration" <?php checked('on',get_option('wa_fb_edd_CompleteRegistration'),true,'checked'); ?>/>
        <input type="submit" value="Save" name="wa_fb_edd_submit"/>
        <?php // submit_button();?>
    </form>
</div>
