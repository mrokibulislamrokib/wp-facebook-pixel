<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       https://webappick.com
 * @since      1.0.0
 *
 * @package    Wa_Fb_Pixel
 * @subpackage Wa_Fb_Pixel/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Wa_Fb_Pixel
 * @subpackage Wa_Fb_Pixel/admin
 * @author     WebAppick <dev@webappick.com>
 */
class Wa_Fb_Pixel_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Wa_Fb_Pixel_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Wa_Fb_Pixel_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/wa-fb-pixel-admin.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Wa_Fb_Pixel_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Wa_Fb_Pixel_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/wa-fb-pixel-admin.js', array( 'jquery' ), $this->version, false );

	}

    /**
     *  REGISTER  Admin Menu area.
     *
     * @since    1.0.0
     */

    public function  admin_menu(){
        add_menu_page("Webappick Facebook Pixel", "Facebook Pixel", "manage_options", "wa_fb_pixel", "webappick_fb_setup", "dashicons-facebook-alt", 99);
       // add_menu_page("Theme Panel", "Theme Panel", "manage_options", "theme-panel", "theme_settings_page", null, 99);
        add_submenu_page("wa_fb_pixel",'Facebook WooSettings','Facebook WooSettings',"manage_options","wa-fb-woo-pixel","wa_fb_pixel_woo_setup");
        add_submenu_page("wa_fb_pixel",'Facebook EDDSettings','Facebook EDDSettings',"manage_options","wa-fb-edd-pixel","wa_fb_pixel_edd_setup");
        add_submenu_page("wa_fb_pixel",'Facebook Custom Event','Facebook Custom Event',"manage_options","wa-fb-custom-pixel","wa_fb_pixel_custom_setup");
    }
	
	
	
	

}
