<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              https://webappick.com
 * @since             1.0.0
 * @package           Wa_Fb_Pixel
 *
 * @wordpress-plugin
 * Plugin Name:       WebAppick Facebook Pixel
 * Plugin URI:        https://webappick.com
 * Description:       This is a short description of what the plugin does. It's displayed in the WordPress admin area.
 * Version:           1.0.0
 * Author:            WebAppick
 * Author URI:        https://webappick.com
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       wa-fb-pixel
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

include_once( ABSPATH . 'wp-admin/includes/plugin.php' );

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-wa-fb-pixel-activator.php
 */
function activate_wa_fb_pixel() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-wa-fb-pixel-activator.php';
	Wa_Fb_Pixel_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-wa-fb-pixel-deactivator.php
 */
function deactivate_wa_fb_pixel() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-wa-fb-pixel-deactivator.php';
	Wa_Fb_Pixel_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_wa_fb_pixel' );
register_deactivation_hook( __FILE__, 'deactivate_wa_fb_pixel' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-wa-fb-pixel.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_wa_fb_pixel() {

	$plugin = new Wa_Fb_Pixel();
	$plugin->run();

}

run_wa_fb_pixel();

function webappick_fb_setup(){
    if(isset($_POST['wa_fb_pixel_id'])){
        update_option('wa_fb_pixel_id',sanitize_text_field($_POST['wa_fb_pixel_id']));
    }
    require plugin_dir_path( __FILE__ ) . 'admin/partials/wa-fb-pixel-admin-display.php';
}

function wa_fb_pixel_woo_setup()
{

    if (isset($_POST['wa_fb_submit'])){
        if (isset($_POST['wa_fb_type'])) {
            update_option('wa_fb_type', sanitize_text_field($_POST['wa_fb_type']));
        }
        update_option("wa_fb_searchcontent", (isset($_POST['wa_fb_searchcontent']) ? "on" : "off"));
        update_option("wa_fb_addtocart", (isset($_POST['wa_fb_addtocart']) ? "on" : "off"));
        update_option("wa_fb_viewcontent", (isset($_POST['wa_fb_viewcontent']) ? "on" : "off"));
        update_option("wa_fb_addwishlist", (isset($_POST['wa_fb_addwishlist']) ? "on" : "off"));
        update_option("wa_fb_InitiateCheckout", (isset($_POST['wa_fb_InitiateCheckout']) ? "on" : "off"));
        update_option("wa_fb_AddPaymentInfo", (isset($_POST['wa_fb_AddPaymentInfo']) ? "on" : "off"));
        update_option("wa_fb_Purchase", (isset($_POST['wa_fb_Purchase']) ? "on" : "off"));
        update_option("wa_fb_Lead", (isset($_POST['wa_fb_Lead']) ? "on" : "off"));
        update_option("wa_fb_CompleteRegistration", (isset($_POST['wa_fb_CompleteRegistration']) ? "on" : "off"));
    }
    if(is_plugin_active( 'woocommerce/woocommerce.php' ) ) {
        require plugin_dir_path( __FILE__ ) . 'admin/partials/wa-fb-pixel-woocommerce-settings.php';
    }else {

    }

}

function wa_fb_pixel_edd_setup(){

    if (isset($_POST['wa_fb_edd_submit'])){
        if (isset($_POST['wa_fb_edd_type'])) {
            update_option('wa_fb_edd_type', sanitize_text_field($_POST['wa_fb_edd_type']));
        }
        update_option("wa_fb_edd_searchcontent", (isset($_POST['wa_fb_edd_searchcontent']) ? "on" : "off"));
        update_option("wa_fb_edd_addtocart", (isset($_POST['wa_fb_edd_addtocart']) ? "on" : "off"));
        update_option("wa_fb_edd_viewcontent", (isset($_POST['wa_fb_edd_viewcontent']) ? "on" : "off"));
        update_option("wa_fb_edd_addwishlist", (isset($_POST['wa_fb_edd_addwishlist']) ? "on" : "off"));
        update_option("wa_fb_edd_InitiateCheckout", (isset($_POST['wa_fb_edd_InitiateCheckout']) ? "on" : "off"));
        update_option("wa_fb_edd_AddPaymentInfo", (isset($_POST['wa_fb_edd_AddPaymentInfo']) ? "on" : "off"));
        update_option("wa_fb_edd_Purchase", (isset($_POST['wa_fb_edd_Purchase']) ? "on" : "off"));
        update_option("wa_fb_edd_Lead", (isset($_POST['wa_fb_edd_Lead']) ? "on" : "off"));
        update_option("wa_fb_edd_CompleteRegistration", (isset($_POST['wa_fb_edd_CompleteRegistration']) ? "on" : "off"));
    }

    if( is_plugin_active( 'easy-digital-downloads/easy-digital-downloads.php')){
        require plugin_dir_path( __FILE__ ) . 'admin/partials/wa-fb-pixel-edd-settings.php';
    }

}

function wa_fb_pixel_custom_setup(){
    require plugin_dir_path( __FILE__ ) . 'admin/partials/wa-fb-pixel-customevent-settings.php';
}


function hook_javascript() {
    $fb_Pixel_id=get_option('wa_fb_pixel_id');
    ?>
    <script>
        !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
            n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
            n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
            t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
            document,'script','https://connect.facebook.net/en_US/fbevents.js');
        fbq('init', <?php echo $fb_Pixel_id;?>); // Insert your pixel ID here.
        fbq('track', 'PageView');
    </script>
    <noscript><img height="1" width="1" style="display:none"
                   src="https://www.facebook.com/tr?id=<?php echo $fb_Pixel_id;?>&ev=PageView&noscript=1"
            /></noscript>
<?php
}
//add_action('wp_head', 'hook_javascript');


function hook_footer_javascript(){
    $fb_Pixel_type = get_option('wa_fb_type');
    $wa_fb_searchcontent = get_option('wa_fb_searchcontent');
    $wa_fb_viewcontent = get_option('wa_fb_viewcontent');
    $wa_fb_addtocart = get_option('wa_fb_addtocart');
    $wa_fb_addwishlist = get_option('wa_fb_addwishlist');
    $wa_fb_InitiateCheckout = get_option('wa_fb_InitiateCheckout');
    $wa_fb_AddPaymentInfo = get_option('wa_fb_AddPaymentInfo');
    $wa_fb_Purchase = get_option('wa_fb_Purchase');
    $wa_fb_Lead = get_option('wa_fb_Lead');
    $wa_fb_CompleteRegistration = get_option('wa_fb_CompleteRegistration');
    if ($fb_Pixel_type == 1) {?>

        <script>
            <?php if($wa_fb_searchcontent=='on') { ?>
            fbq('track', 'Search');
            <?php } ?>
            <?php if($wa_fb_viewcontent=='on') { ?>
            fbq('track', 'ViewContent');
            <?php } ?>
            <?php if($wa_fb_addtocart=='on') { ?>
            fbq('track', 'AddToCart');
            <?php } ?>
            <?php if($wa_fb_InitiateCheckout=='on') { ?>
            fbq('track', 'InitiateCheckout');
            <?php } ?>
            <?php if($wa_fb_AddPaymentInfo=='on') { ?>
            fbq('track', 'AddPaymentInfo');
            <?php } ?>
            <?php if($wa_fb_Purchase=='on') { ?>
            fbq('track', 'Purchase');
            <?php } ?>
        </script>

    <?php } elseif ($fb_Pixel_type == 2) { ?>

        <script>
            <?php if($wa_fb_searchcontent=='on') {
                $product_id_array=array();
                if(is_search()){
                    $search_query= get_search_query();
            ?>
            fbq('track', 'Search', {
                search_string: "<?php echo $search_query;?>"
            });
            <?php } } ?>

            <?php if($wa_fb_viewcontent=='on') {
                     if(is_product()){
                        $single_product_id=get_queried_object_id();
                        $_product = wc_get_product($single_product_id);
                        $single_product_total=$_product->get_price();
                        $single_product_array=array('value'=>$single_product_total,'currency'=>get_option('woocommerce_currency'));

            ?>

            var rok =<?php echo json_encode($single_product_array);?>;

            fbq('track', 'ViewContent', {
                value:rok.value,
                currency:rok.currency
            });


            <?php } } ?>

            <?php if($wa_fb_addtocart=='on') {
                    global $woocommerce;
                    $items = $woocommerce->cart->get_cart();
                    $ids = array();
                    foreach($items as $item => $values) {
                            $_product = $values['data']->post;
                            $ids[] = $_product->ID;
                    }
                    $last_cart_product_id=end($ids);
                    $_product = wc_get_product($last_cart_product_id);
                    $total=$_product->get_price();
                    $cart_array=array('value'=>$total,'currency'=>get_option('woocommerce_currency'));
             ?>
            var rok =<?php echo json_encode($cart_array);?>;
            fbq('track', 'AddToCart', {
                value:rok.value,
                currency:rok.currency
            });
            <?php } ?>


            <?php if($wa_fb_InitiateCheckout=='on') { if(is_checkout()){  ?>
            fbq('track', 'InitiateCheckout');
            <?php } } ?>

            <?php if($wa_fb_AddPaymentInfo=='on') { if(is_checkout()){ ?>
            fbq('track', 'AddPaymentInfo');
            <?php } } ?>

            <?php if($wa_fb_Purchase=='on') { ?>

            <?php } ?>


        </script>


    <?php } elseif ($fb_Pixel_type == 3) { ?>

        <script>

            <?php if($wa_fb_searchcontent=='on') {
                $product_id_array=array();
                if(is_search()){
                    $search_query= get_search_query();
                    $params = array('s' => $search_query,'post_type'=>'product');
                    $wc_query = new WP_Query($params);
                    if ($wc_query->have_posts()) :
                        while ($wc_query->have_posts()) :
                            $wc_query->the_post();
                                array_push($product_id_array,get_the_ID());
                        endwhile;
                        wp_reset_postdata();
                    else:

                    endif;

            $product_search__array=array("search_string"=>$search_query,'content_ids'=>json_encode($product_id_array),'content_type'=> 'product');

            ?>

            var rok =<?php echo json_encode($product_search__array);?>;


            fbq('track', 'Search', {
                search_string: rok.search_string,
                content_ids: rok.content_ids,
                content_type: 'product'
            });

            <?php } } ?>

            <?php if($wa_fb_viewcontent=='on') {
              if(is_product()){
                    $single_product_id=get_queried_object_id();
                    $_product = wc_get_product($single_product_id);
                    $total=$_product->get_price();
                    $product_array=array('content_ids'=>$single_product_id,'content_type'=> 'product','value'=>$total,'currency'=>get_option('woocommerce_currency'));
            ?>
            var rok =<?php echo json_encode($product_array);?>;
            fbq('track', 'ViewContent', {
                content_ids:rok.content_ids,
                content_type:rok.content_type,
                value:rok.value,
                currency:rok.currency
            });

            <?php } } ?>

            <?php if($wa_fb_addtocart=='on') {
                    global $woocommerce;
                    $total=$woocommerce->cart->subtotal;
                    $product_id_array=array();
                    foreach(WC()->cart->get_cart() as $cart_item ){
                        array_push($product_id_array,$cart_item['product_id']);
                    }
                    $cart_array=array('content_ids'=>json_encode($product_id_array),'content_type'=> 'product','value'=>$total,'currency'=>get_option('woocommerce_currency'));
            ?>
            var rok =<?php echo json_encode($cart_array);?>;
            var kok=JSON.stringify(rok);
            console.log(kok);
            fbq('track', 'AddToCart',{
                    content_ids:rok.content_ids,
                    content_type:rok.content_type,
                    value:rok.value,
                    currency:rok.currency
                }
            );
            <?php } ?>



            <?php if($wa_fb_InitiateCheckout=='on') {if(is_checkout()){ ?>
            fbq('track', 'InitiateCheckout');
            <?php } } ?>

            <?php if($wa_fb_AddPaymentInfo=='on') {
                    if(is_checkout()){  ?>
            fbq('track', 'AddPaymentInfo');
            <?php } }?>

            <?php if($wa_fb_Purchase=='on') { ?>

                   /* add_action('woocommerce_thankyou', 'wh_CustomReadOrder');

                    function wh_CustomReadOrder($order_id){
                        $order = wc_get_order($order_id);
                        $total=$order->get_total();
                    }

                    add_action('woocommerce_order_status_completed','insert_points',10,1);

                    function insert_points($order_id){
                        $order = wc_get_order($order_id);
                        $total=$order->get_total();
                    }
                    *//*

             fbq('track', 'Purchase', {
             content_ids: ['1234', '4642', '35838'],
             content_type: 'product',
             value: '',
             currency: 'USD'
             });*/

            <?php } ?>



            <?php if($wa_fb_CompleteRegistration=='on') {

                add_action('user_register','my_function');

                function my_function($user_id){ ?>

            fbq('track', 'CompleteRegistration', {
                value: 25.00,
                currency: 'USD'
            });


            <?php } } ?>

        </script>



    <?php
    }

}

//add_action('wp_footer', 'hook_footer_javascript');




function hook_footer_edd_javascript(){


}


//add_action('wp_footer', 'hook_footer_edd_javascript');

?>