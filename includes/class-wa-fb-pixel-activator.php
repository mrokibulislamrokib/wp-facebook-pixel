<?php

/**
 * Fired during plugin activation
 *
 * @link       https://webappick.com
 * @since      1.0.0
 *
 * @package    Wa_Fb_Pixel
 * @subpackage Wa_Fb_Pixel/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Wa_Fb_Pixel
 * @subpackage Wa_Fb_Pixel/includes
 * @author     WebAppick <dev@webappick.com>
 */
class Wa_Fb_Pixel_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}
