<?php

/**
 * Define the internationalization functionality
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @link       https://webappick.com
 * @since      1.0.0
 *
 * @package    Wa_Fb_Pixel
 * @subpackage Wa_Fb_Pixel/includes
 */

/**
 * Define the internationalization functionality.
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @since      1.0.0
 * @package    Wa_Fb_Pixel
 * @subpackage Wa_Fb_Pixel/includes
 * @author     WebAppick <dev@webappick.com>
 */
class Wa_Fb_Pixel_i18n {


	/**
	 * Load the plugin text domain for translation.
	 *
	 * @since    1.0.0
	 */
	public function load_plugin_textdomain() {

		load_plugin_textdomain(
			'wa-fb-pixel',
			false,
			dirname( dirname( plugin_basename( __FILE__ ) ) ) . '/languages/'
		);

	}



}
