<?php

/**
 * Fired during plugin deactivation
 *
 * @link       https://webappick.com
 * @since      1.0.0
 *
 * @package    Wa_Fb_Pixel
 * @subpackage Wa_Fb_Pixel/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Wa_Fb_Pixel
 * @subpackage Wa_Fb_Pixel/includes
 * @author     WebAppick <dev@webappick.com>
 */
class Wa_Fb_Pixel_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
